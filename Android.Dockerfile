FROM ubuntu:18.04

# Install required dependencies
RUN apt-get update && \
    apt-get install -y --no-install-recommends &&\
    apt-get install zipalign -y && \
    apt-get install -y apksigner \
    wget \
    unzip \
    openjdk-17-jdk-headless \
    dos2unix \
    python3

# Download and install SDK Manager
RUN wget https://dl.google.com/android/repository/commandlinetools-linux-7583922_latest.zip -O sdk-tools.zip && \
    unzip sdk-tools.zip -d /opt/android-sdk

# Set up environment variables
ENV ANDROID_HOME /opt/android-sdk
ENV PATH $ANDROID_HOME/cmdline-tools/tools/bin:$PATH

# Accept licenses
RUN mkdir -p $ANDROID_HOME/licenses && \
    echo "24333f8a63b6825ea9c5514f83c2829b004d1fee" > $ANDROID_HOME/licenses/android-sdk-license

# Set Terminal environment variables
ENV TERMINAL_KEYSTORE_PASS="passKeystore"
ENV TERMINAL_ALIAS="aliaskey"
ENV TERMINAL_KEY_PASS="keyPass"

WORKDIR /app

# Copy the app
COPY ./Terminal .

# Convert line endings of the gradlew script
RUN dos2unix gradlew && chmod +x gradlew

# Build the release APK
RUN ./gradlew assembleRelease  --stacktrace --no-daemon

# Set the APK file path
ENV APK_FILE "app/build/outputs/apk/release/app-release-unsigned.apk"
ENV SIGNED_APK_FILE "app-release.apk"
ENV ALIGNED_APK_FILE "app-release-aligned.apk"

# Generate the keystore
RUN keytool -genkey -V \
        -keystore $ANDROID_HOME/keystore.jks \
        -keyalg RSA \
        -keysize 2048 \
        -validity 10000 \
        -storepass $TERMINAL_KEYSTORE_PASS \
        -keypass $TERMINAL_KEY_PASS \
        -dname "CN=Your Name, OU=Your Organizational Unit, O=Your Organization, L=Your City, ST=Your State, C=Your Country" \
        -alias $TERMINAL_ALIAS


RUN jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1  -keystore $ANDROID_HOME/keystore.jks  -storepass $TERMINAL_KEYSTORE_PASS -keypass $TERMINAL_KEYSTORE_PASS  $APK_FILE $TERMINAL_ALIAS 
# Sign the APK with jarsigner 
#RUN jarsigner -verbose -sigalg SHA1withRSA -digestalg SHA1 \
 #   -keystore $ANDROID_HOME/keystore.jks \
  #  -storepass $TERMINAL_KEYSTORE_PASS \
   # -keypass pass \
   # $APK_FILE alias
#RUN jarsigner -verbose -sigalg SHA1withRSA -digesSIGNED_APK_FILEtalg SHA1 \
 #   -keystore $ANDROID_HOME/keystore.jks \
  #  -storepass $TERMINAL_KEYSTORE_PASS \
  #  -signedjar $SIGNED_APK_FILE \
   # $APK_FILE $TERMINAL_ALIAS

# Optimize the APK with zipalign
RUN zipalign -v 4 $APK_FILE  terminal-signed.apk

# Verify the signature

# Display the signed APK details

# Clean up unnecessary files

# Start an HTTP server
CMD python3 -m http.server 8080

# Expose the port for external access
EXPOSE 8080
