ifndef env
	env=dev
endif

include ./.environment/${env}

build-server:
	docker compose build --progress plain

run: run-db build-terminal-app  build-server start-containers

run-db:
	docker compose up --remove-orphans -d cash-manager-db

rm-vol-db:
	docker volume rm cashmanager_postgres-data

db-down:
	docker compose down

start-containers:
	docker compose up -d  cashmanager-server terminal-app 

build-terminal-app:
	docker build -t terminal-app -f Android.Dockerfile . --progress plain
