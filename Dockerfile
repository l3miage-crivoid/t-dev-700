FROM maven:3.8.3-openjdk-17 as build
ARG PROFILE
WORKDIR /usr/src/app
COPY . .
RUN mvn -f ./cashmanager/pom.xml clean package ${PROFILE} -DskipTests

FROM openjdk:17-oracle
ARG PROFILE
COPY --from=build /usr/src/app/cashmanager/target/cash-manager.jar /usr/local/lib/cash-manager.jar
EXPOSE 8443 8080
ENTRYPOINT java ${PROFILE} -jar /usr/local/lib/cash-manager.jar
