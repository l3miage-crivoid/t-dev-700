# T-DEV-700-LYO_2
CachManager : distant payment system for retailer like business
Main hardware : smartphone

Project management : https://trello.com/invite/b/Uz8XQ05R/ATTIe08698d3710f412f16633dba31118ae3164664D7/features

Intaller docker
Installer Maven
Creer base de donnée "test"
Lancer l'image DB
Installer android studio sur le PC
Mettre le smartphone en mode developpeur pour en faire le terminal
Activer la lecture NFC sur le terminal
Lancer appli depuis le PC sur le terminal
Verifier que le terminal detecte une puce NFC
Verifier que le terminal communique avec la base de donnée des stocks
Verifier que le terminal communique avec la base de donnée de la banque
