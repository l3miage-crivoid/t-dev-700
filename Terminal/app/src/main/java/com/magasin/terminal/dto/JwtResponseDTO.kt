package com.magasin.terminal.dto

data class JwtResponseDTO(
    val jwt: String,
    val id: Long,
    val username: String,
    val roles: Set<String>
)
