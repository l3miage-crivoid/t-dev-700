package com.magasin.terminal

import android.content.Intent
import android.content.SharedPreferences
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.CompoundButton
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.magasin.terminal.databinding.ActivityBinder
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import java.text.NumberFormat
import java.util.Locale

public class MainActivity : AppCompatActivity, CompoundButton.OnCheckedChangeListener,
    NfcAdapter.ReaderCallback {

    companion object {
        private val TAG = MainActivity::class.java.simpleName
    }

    private var binder: ActivityBinder? = null
    private val viewModel: MainViewModel by lazy { ViewModelProvider(this).get(MainViewModel::class.java) }
    private lateinit var mainViewModel: MainViewModel
    private lateinit var cartViewModel: CartViewModel

    constructor() {

    }

    override fun onCreate(savedInstanceState: Bundle?) {
        binder = DataBindingUtil.setContentView(this@MainActivity, R.layout.activity_main)
        binder?.setViewModel(viewModel)
        binder?.setLifecycleOwner(this@MainActivity)
        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]
        cartViewModel = ViewModelProvider(this)[CartViewModel::class.java]
        super.onCreate(savedInstanceState)
        binder?.toggleButton?.setOnCheckedChangeListener(this@MainActivity)
        binder?.recyclerView?.adapter = viewModel.stockListAdapter
        binder?.recyclerView?.layoutManager = LinearLayoutManager(this)

        //When clicking on item in list incrementing price counter and item counter
        viewModel.cartViewModel?.cartTotal?.observe(this) { total ->
            // Update the UI with the cumulative total
            val totalText =
                "Items: ${viewModel.cartViewModel!!.selectedItemsCount.value} - ${total.formatAsCurrency()}"
            binder?.textViewSelectedCount?.text = totalText
        }

        // Set the click listener for your adding price to cart
        viewModel.stockListAdapter.setOnItemClickListener { stock ->
            viewModel.cartViewModel?.addToCart(stock)
        }

        //Deactivate visibility of the stock if !isChecked
        viewModel.showItemContent.observe(this) { isChecked ->
            viewModel.initialVisibility.value = isChecked
        }

        // catch if backend sended 401 then go to Login
        mainViewModel.navigateToLogin.observe(this) { shouldNavigateToLogin ->
            if (shouldNavigateToLogin) {
                navigateToLoginActivity()
            }
        }

        Coroutines.main(this@MainActivity) { scope ->
            scope.launch(block = {
                binder?.getViewModel()?.observeNFCStatus()?.collectLatest(action = { status ->
                    Log.d(TAG, "observeNFCStatus $status")
                    if (status == NFCStatus.NoOperation) NFCManager.disableReaderMode(
                        this@MainActivity,
                        this@MainActivity
                    )
                    else if (status == NFCStatus.Tap) NFCManager.enableReaderMode(
                        this@MainActivity,
                        this@MainActivity,
                        this@MainActivity,
                        viewModel.getNFCFlags(),
                        viewModel.getExtras()
                    )
                })
            })
            scope.launch(block = {
                binder?.viewModel?.observeToast()?.collectLatest(action = { message ->
                    Log.d(TAG, "observeToast $message")
                    Toast.makeText(this@MainActivity, message, Toast.LENGTH_LONG).show()
                })
            })
        }
    }

    private fun navigateToLoginActivity() {
        Toast.makeText(this@MainActivity, "Session Expired", Toast.LENGTH_LONG).show()
        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
        finish() // Close MainActivity to prevent returning to it without logging in again
    }

    fun Double.formatAsCurrency(): String {
        val currencyFormat = NumberFormat.getCurrencyInstance(Locale.getDefault())
        return currencyFormat.format(this)
    }

    /**
     * Overrides the onCheckedChanged method from CompoundButton.OnCheckedChangeListener.
     * This method is called when the state of a CompoundButton (e.g., ToggleButton) changes.
     *
     * @param buttonView The CompoundButton whose state has changed.
     * @param isChecked The new checked state of buttonView (true if checked, false if unchecked).
     */
    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        if (buttonView == binder?.toggleButton) {
            viewModel.onCheckNFC(isChecked)
            if (isChecked) {
                // Add or replace the fragment
                supportFragmentManager.beginTransaction()
                    .replace(R.id.fragment_container, MainFragment.newInstance())
                    .commit()
            } else if(viewModel.isPaymentSuccessful && !isChecked) {
                // Remove the fragment
                viewModel.cartViewModel?.clearCart()
                viewModel.setPaymentSuccess(false)
            }
            else{
                supportFragmentManager.findFragmentById(R.id.fragment_container)?.let {
                    supportFragmentManager.beginTransaction().remove(it).commit()
                }
            }
        }
    }

    override fun onTagDiscovered(tag: Tag?) {
        binder?.getViewModel()?.readTag(tag)
    }

    override fun onDestroy() {
        super.onDestroy()
        mainViewModel.onDestroy()
    }

}
