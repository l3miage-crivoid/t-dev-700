package com.magasin.terminal.dto

data class LoginDTO(
    val username: String,
    val password: String
)