package com.magasin.terminal.service

import com.magasin.terminal.dto.Stock
import retrofit2.Response
import retrofit2.http.GET

interface StockService {
    @GET("api/stock/get-stock")
    suspend fun getStock(): Response<List<Stock>>
}