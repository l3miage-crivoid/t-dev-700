package com.magasin.terminal.service

import com.magasin.terminal.dto.SubstractOperationDTO
import com.magasin.terminal.dto.TransactionResponseDTO
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface BankAccountService {
    @POST("api/bank/proceed-payment")
    suspend fun proceedPayment(@Body dto: SubstractOperationDTO): Response<TransactionResponseDTO>
}