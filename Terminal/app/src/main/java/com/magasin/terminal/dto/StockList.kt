package com.magasin.terminal.dto

data class StockList(
    val stocks: List<Stock>
)