package com.magasin.terminal

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.magasin.terminal.databinding.FragmentBinder
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

/**
 * MainFragment is responsible for displaying the main user interface related to payment processing.
 * It observes LiveData from MainViewModel to update the UI accordingly based on the payment status.
 */
class MainFragment : Fragment {

    companion object {
        /**
         * Factory method to create a new instance of MainFragment.
         * @return A new instance of MainFragment.
         */
        fun newInstance(): MainFragment = MainFragment()
    }

    // Binder to reference the views in the associated XML layout.
    private var binder: FragmentBinder? = null

    // ViewModel that holds the UI-related data and handles business logic.
    private val viewModel: MainViewModel by lazy {
        ViewModelProvider(requireActivity())[MainViewModel::class.java]
    }

    // ViewModel for cart-related operations and data.
    private lateinit var cartViewModel: CartViewModel

    // Empty constructor for the fragment.
    constructor()

    /**
     * Called to have the fragment instantiate its user interface view.
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment.
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to.
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous saved state.
     * @return The View for the fragment's UI, or null.
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        // Inflate the layout for this fragment with data binding.
        binder = DataBindingUtil.inflate(inflater, R.layout.fragment_main, container, false)
        // Bind the ViewModel to the layout.
        binder?.setViewModel(viewModel)
        // Set the lifecycle owner to manage LiveData lifecycle.
        binder?.setLifecycleOwner(this@MainFragment)
        // Initialize the CartViewModel.
        cartViewModel = ViewModelProvider(this)[CartViewModel::class.java]
        return binder?.root
    }

    /**
     * Called immediately after onCreateView has returned, but before any saved state has been restored into the view.
     * @param view The View returned by onCreateView.
     * @param savedInstanceState If non-null, this fragment is re-constructed from a previous saved state.
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Observe the success message when paying LiveData and update the UI.
        viewModel.paymentSuccessMessage.observe(viewLifecycleOwner) { successMessage ->
            successMessage?.let {
                binder?.textViewPrompt?.text = it
                binder?.textViewPrompt?.setTextColor(ContextCompat.getColor(requireContext(), R.color.green))
                binder?.textViewWaitingForPayment?.text = "Payment completed."
                binder?.textViewWaitingForPayment?.setTextColor(ContextCompat.getColor(requireContext(), R.color.green))
            }
        }

        // Observe the error message LiveData and update the UI.
        viewModel.paymentErrorMessage.observe(viewLifecycleOwner) { errorMessage ->
            errorMessage?.let {
                binder?.textViewPrompt?.text = it
                binder?.textViewPrompt?.setTextColor(ContextCompat.getColor(requireContext(), R.color.red))
            }
        }

        // Coroutine scopes for observing LiveData from the ViewModel.
        Coroutines.main(this@MainFragment) { scope ->
            scope.launch {
                binder?.viewModel?.observeToast()?.collectLatest { message ->
                    Toast.makeText(requireContext(), message, Toast.LENGTH_LONG).show()
                }
            }
            scope.launch {
                binder?.getViewModel()?.observeLivePayment()?.collectLatest { livePayment ->
                    livePayment?.let {
                        binder?.textViewPrompt?.text = it
                        binder?.textViewWaitingForPayment?.text = "Waiting for payment ..."
                        binder?.textViewWaitingForPayment?.setTextColor(ContextCompat.getColor(requireContext(), R.color.white))
                    }
                }
            }
        }

        super.onViewCreated(view, savedInstanceState)
    }
}