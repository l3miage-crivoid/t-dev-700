package com.magasin.terminal

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.magasin.terminal.dto.JwtResponseDTO
import com.magasin.terminal.dto.LoginDTO
import com.magasin.terminal.service.AccountService
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/**
 * Activity for handling user login.
 */
class LoginActivity : AppCompatActivity() {

    // Declaration of UI components
    private lateinit var usernameEditText: EditText
    private lateinit var passwordEditText: EditText
    private lateinit var loginButton: Button

    // Setup Retrofit instance for network operations
    val retrofit = Retrofit.Builder()
        .baseUrl("http://164.92.229.245:8080/") // Replace with your server URL
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    // Create a service for account-related network operations
    val accountService = retrofit.create(AccountService::class.java)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        // Initialize SharedPreferences to store/retrieve JWT token
        val sharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE)
        val jwtToken = sharedPreferences.getString("jwtToken", null)

        // Check if JWT Token already exists and navigate to MainActivity
        if (jwtToken != null) {
            startActivity(Intent(this, MainActivity::class.java))
            finish() // Close LoginActivity
        }

        // Initialize UI components
        usernameEditText = findViewById(R.id.usernameEditText)
        passwordEditText = findViewById(R.id.passwordEditText)
        loginButton = findViewById(R.id.loginButton)

        // Set up click listener for the login button
        loginButton.setOnClickListener {
            performLogin()
        }
    }

    /**
     * Handles the login logic.
     */
    private fun performLogin() {
        val username = usernameEditText.text.toString().trim()
        val password = passwordEditText.text.toString().trim()

        // Validate input fields
        if (username.isEmpty() || password.isEmpty()) {
            Toast.makeText(this, "Username and password must not be empty", Toast.LENGTH_SHORT).show()
            return
        }

        // Create DTO for login credentials
        val loginDTO = LoginDTO(username, password)

        // Perform network request on IO thread
        Coroutines.io {
            try {
                val response = accountService.loginUser(loginDTO)
                val jwtResponseDTO = response.body()

                // Check for successful response and navigate
                if (response.isSuccessful && jwtResponseDTO != null) {
                    navigateToMainActivity(jwtResponseDTO)
                } else if (response.code() == 401) {
                    // Handle Unauthorized (Invalid credentials)
                    runOnUiThread {
                        Toast.makeText(this@LoginActivity, "Username or password are not valid", Toast.LENGTH_SHORT).show()
                    }
                } else {
                    // Handle other errors
                    runOnUiThread {
                        Toast.makeText(this@LoginActivity, "Login failed", Toast.LENGTH_SHORT).show()
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
                // Handle network or other errors
                runOnUiThread {
                    Toast.makeText(this@LoginActivity, "An error occurred: ${e.message}", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    /**
     * Navigates to the MainActivity after successful login.
     *
     * @param jwtResponse The response containing JWT and other user details.
     */
    private fun navigateToMainActivity(jwtResponse: JwtResponseDTO) {
        val sharedPreferences: SharedPreferences = getSharedPreferences("MySharedPref", MODE_PRIVATE)
        val editor: SharedPreferences.Editor = sharedPreferences.edit()

        // Store JWT and other user details in SharedPreferences
        editor.putString("jwtToken", jwtResponse.jwt)
        editor.putLong("userId", jwtResponse.id)
        editor.putString("username", jwtResponse.username)
        editor.putStringSet("roles", jwtResponse.roles)
        editor.apply()

        // Navigate to MainActivity
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }
}