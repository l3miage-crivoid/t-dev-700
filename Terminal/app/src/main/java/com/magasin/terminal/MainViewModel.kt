package com.magasin.terminal

import android.app.Application
import android.content.ContentValues
import android.content.Context.MODE_PRIVATE
import android.media.MediaPlayer
import android.nfc.NfcAdapter
import android.nfc.Tag
import android.nfc.tech.MifareClassic
import android.nfc.tech.MifareUltralight
import android.os.Bundle
import android.util.Log
import androidx.databinding.BindingAdapter
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import com.magasin.terminal.dto.StockList
import com.magasin.terminal.dto.SubstractOperationDTO
import com.magasin.terminal.dto.TransactionResponseDTO
import com.magasin.terminal.interceptor.BearerTokenInterceptor
import com.magasin.terminal.service.BankAccountService
import com.magasin.terminal.service.StockService
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.MutableSharedFlow
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.SharedFlow
import kotlinx.coroutines.flow.StateFlow
import kotlinx.coroutines.flow.asSharedFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.withContext
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.Date
import kotlin.experimental.and

/**
 * ViewModel for the main activity of the application. Handles UI-related data,
 * NFC functionalities, and interacts with backend services.
 *
 * @param application Application context passed to AndroidViewModel.
 */
public class MainViewModel : AndroidViewModel {

    // LiveData objects for observing changes in UI.
    val stockList: MutableLiveData<StockList> = MutableLiveData()
    val stockListAdapter: StockListAdapter = StockListAdapter()
    var cartViewModel: CartViewModel? = CartViewModel()
    val initialVisibility: MutableLiveData<Boolean> = MutableLiveData()
    val isToggleButtonChecked = MutableLiveData<Boolean>()
    val paymentSuccessMessage: MutableLiveData<String?> = MutableLiveData()
    val paymentErrorMessage: MutableLiveData<String?> = MutableLiveData()
    val showItemContent: LiveData<Boolean> get() = isToggleButtonChecked
    private val _navigateToLogin = MutableLiveData<Boolean>()
    val navigateToLogin: LiveData<Boolean> get() = _navigateToLogin

    // Retrofit setup for network calls.
    private val retrofit: Retrofit
    private var bankAccountService: BankAccountService
    private var stockService: StockService

    // NFC and Toast message handling.
    private val liveNFC: MutableStateFlow<NFCStatus?>
    private val liveToast: MutableSharedFlow<String?>
    private val liveTag: MutableStateFlow<String?>
    private val livePayment: MutableStateFlow<String?>

    // MediaPlayer for playing sounds.
    private var mediaPlayer: MediaPlayer? = null

    var isPaymentSuccessful: Boolean = false

    init {
        initialVisibility.value = true
        val sharedPreferences = getApplication<Application>().getSharedPreferences("MySharedPref", MODE_PRIVATE)
        val token = sharedPreferences.getString("jwtToken", "") ?: ""

        val okHttpClient = OkHttpClient.Builder()
            .addInterceptor(BearerTokenInterceptor(token))
            .build()

        retrofit = Retrofit.Builder()
            .baseUrl("http://164.92.229.245:8080/")
            .addConverterFactory(GsonConverterFactory.create())
            .client(okHttpClient)
            .build()

        bankAccountService = retrofit.create(BankAccountService::class.java)

         stockService = retrofit.create(StockService::class.java)

        Coroutines.default(this@MainViewModel) {
            try {
                val responseStock = stockService.getStock()

                if (responseStock.isSuccessful) {
                    withContext(Dispatchers.Main) {
                        stockList.value = responseStock.body()?.let { StockList(stocks = it) }
                        stockList.observeForever { list ->
                            stockListAdapter.setStock(list)
                        }
                    }
                }
                else if (responseStock.code() == 401) {
                    // Handle Unauthorized (Invalid credentials)
                    withContext(Dispatchers.Main) {
                        val sharedPreferences = getApplication<Application>().getSharedPreferences("MySharedPref", MODE_PRIVATE)
                        with(sharedPreferences.edit()) {
                            remove("jwtToken")
                            apply()
                        }
                        _navigateToLogin.postValue(true)
                    }
                }
                else {
                    Log.e(ErrorStock, "Couldn't retrieve stock")
                }
            } catch (e: Exception) {
                // Handle network or other exceptions
                Log.e(ErrorStock, "Exception during API call: ${e.message}", e)
            }
        }
    }

    companion object {
        private val TAG = MainViewModel::class.java.simpleName
        private val ErrorStock = MainViewModel::class.java.simpleName
        private const val prefix = "android.nfc.tech."

        @BindingAdapter("stockList")
        @JvmStatic
        fun setStockList(recyclerView: RecyclerView, stockList: StockList?) {
            stockList?.let {
                (recyclerView.adapter as? StockListAdapter)?.setStock(it)
            }
        }
    }



    constructor( application: Application) : super(application) {
        Log.d(TAG, "constructor")
        liveNFC = MutableStateFlow(null)
        liveToast = MutableSharedFlow()
        liveTag = MutableStateFlow(null)
        livePayment = MutableStateFlow(null)
    }


    private suspend fun postToast(message: String) {
        Log.d(TAG, "postToast(${message})")
        liveToast.emit(message)
    }

    fun observeToast(): SharedFlow<String?> {
        return liveToast.asSharedFlow()
    }

    //endregion
    fun getNFCFlags(): Int {
        return NfcAdapter.FLAG_READER_NFC_A or
                NfcAdapter.FLAG_READER_NFC_B or
                NfcAdapter.FLAG_READER_NFC_F or
                NfcAdapter.FLAG_READER_NFC_V or
                NfcAdapter.FLAG_READER_NFC_BARCODE //or NfcAdapter.FLAG_READER_SKIP_NDEF_CHECK
    }

    fun getExtras(): Bundle {
        val options: Bundle = Bundle();
        options.putInt(NfcAdapter.EXTRA_READER_PRESENCE_CHECK_DELAY, 30000);
        return options
    }

    //region NFC Methods
    fun onCheckNFC(isChecked: Boolean) {
        isToggleButtonChecked.value = !isChecked
        Coroutines.io(this@MainViewModel) {
            Log.d(TAG, "onCheckNFC(${isChecked})")
            if (isChecked) {
                postNFCStatus(NFCStatus.Tap)
            } else {
                withContext(Dispatchers.Main) {
                    paymentSuccessMessage.value = null
                    paymentErrorMessage.value = null
                    livePayment.emit("Put your card against device to process payment")
                }
                postToast("NFC is Disabled, Please Toggle On!")
            }
        }
    }

    private fun playSound() {
        try {
            if (mediaPlayer == null) {
                mediaPlayer = MediaPlayer.create(getApplication(), R.raw.cash_beep)
            }

            mediaPlayer?.start()
        } catch (e: Exception) {
            Log.e(TAG, "Error playing sound: ${e.message}", e)
        }
    }

    //region NFC and Payment Methods

    /**
     * Processes the NFC tag, performs payment operations and updates UI accordingly.
     *
     * @param tag The NFC tag detected by the device.
     */
    fun readTag(tag: Tag?) {
        Coroutines.default(this@MainViewModel) {
            playSound()
            Log.d(TAG, "readTag(${tag} ${tag?.getTechList()})")
            postNFCStatus(NFCStatus.Process)
            val stringBuilder: StringBuilder = StringBuilder()
            val id: ByteArray? = tag?.getId()
            stringBuilder.append("Tag ID (hex): ${getHex(id!!)} \n")
            stringBuilder.append("Tag ID (dec): ${getDec(id)} \n")
            stringBuilder.append("Tag ID (reversed): ${getReversed(id)} \n")
            stringBuilder.append("Technologies: ")
            tag.techList.forEach { tech ->
                stringBuilder.append(tech.substring(prefix.length))
                stringBuilder.append(", ")
            }
            stringBuilder.delete(stringBuilder.length - 2, stringBuilder.length)
            tag.techList.forEach { tech ->
                if (tech.equals(MifareClassic::class.java.getName())) {
                    stringBuilder.append('\n')
                    val mifareTag: MifareClassic = MifareClassic.get(tag)
                    val type: String
                    if (mifareTag.type == MifareClassic.TYPE_CLASSIC) type = "Classic"
                    else if (mifareTag.type == MifareClassic.TYPE_PLUS) type = "Plus"
                    else if (mifareTag.type == MifareClassic.TYPE_PRO) type = "Pro"
                    else type = "Unknown"
                    stringBuilder.append("Mifare Classic type: $type \n")
                    stringBuilder.append("Mifare size: ${mifareTag.getSize()} bytes \n")
                    stringBuilder.append("Mifare sectors: ${mifareTag.getSectorCount()} \n")
                    stringBuilder.append("Mifare blocks: ${mifareTag.getBlockCount()}")
                }
                if (tech.equals(MifareUltralight::class.java.getName())) {
                    stringBuilder.append('\n');
                    val mifareUlTag: MifareUltralight = MifareUltralight.get(tag);
                    val type: String
                    if (mifareUlTag.getType() == MifareUltralight.TYPE_ULTRALIGHT) type =
                        "Ultralight"
                    else if (mifareUlTag.getType() == MifareUltralight.TYPE_ULTRALIGHT_C) type =
                        "Ultralight C"
                    else type = "Unkown"
                    stringBuilder.append("Mifare Ultralight type: ");
                    stringBuilder.append(type)
                }
            }
            Log.d(TAG, "Datum: $stringBuilder")
            Log.d(ContentValues.TAG, "dumpTagData Return \n $stringBuilder")
            postNFCStatus(NFCStatus.Read)

            // Access the current value of cartTotal directly
            val totalAmount = cartViewModel?.cartTotal?.value ?: 0.0
            val uidHexString = getHex(tag?.id!!)

            val substractOperationDTO = SubstractOperationDTO(
                uid = uidHexString,
                amountToSubstract = totalAmount
            )

            try {
                val response = bankAccountService.proceedPayment(substractOperationDTO)

                if (response.isSuccessful) {
                    // Handle successful response
                    val transactionResponseDTO = response.body()
                    if (transactionResponseDTO != null) {
                        setPaymentSuccess(true)
                        Log.d(TAG, "API call successful: ${transactionResponseDTO.message}")
                        stringBuilder.append("\n${transactionResponseDTO.message} with amount ${transactionResponseDTO.amountToSubstract}")
                        paymentSuccessMessage.postValue("Card accepted")
                    } else {
                        setPaymentSuccess(false)
                        Log.e(TAG, "Response body is null")
                    }
                }
                else if (response.code() == 401) {
                    // Handle Unauthorized (Invalid credentials)
                    withContext(Dispatchers.Main) {
                        val sharedPreferences = getApplication<Application>().getSharedPreferences("MySharedPref", MODE_PRIVATE)
                        with(sharedPreferences.edit()) {
                            remove("jwtToken")
                            apply()
                        }
                        _navigateToLogin.postValue(true)
                    }
                }
                else {
                    paymentErrorMessage.postValue("Card refused please try again")
                    val errorBodyString = response.errorBody()?.string()
                    val errorResponseDTO =
                        Gson().fromJson(errorBodyString, TransactionResponseDTO::class.java)

                    if (errorResponseDTO != null) {
                        Log.e(
                            TAG,
                            "API successful call failed: ${response.code()} - ${errorResponseDTO.message}"
                        )
                        stringBuilder.append("\nError: ${errorResponseDTO.message}")
                        liveTag.emit("${getDateTimeNow()} \n $stringBuilder")
                    } else {
                        Log.e(TAG, "Error response body is null")
                    }
                }
            } catch (e: Exception) {
                // Handle network or other exceptions
                Log.e(TAG, "Exception during API call: ${e.message}", e)
            }
        }
    }

    /**
     * Posts NFC status to the UI.
     *
     * @param status The current NFC status.
     */
    private suspend fun postNFCStatus(status: NFCStatus) {
        Log.d(TAG, "postNFCStatus(${status})")
        if (NFCManager.isSupportedAndEnabled(getApplication())) {
            liveNFC.emit(status)
        } else if (NFCManager.isNotEnabled(getApplication())) {
            liveNFC.emit(NFCStatus.NotEnabled)
            postToast("Please Enable your NFC!")
            liveTag.emit("Please Enable your NFC!")
        } else if (NFCManager.isNotSupported(getApplication())) {
            withContext(Dispatchers.Main) {
                paymentErrorMessage.value = "Card refused please try again"
            }
            liveNFC.emit(NFCStatus.NotSupported)
            postToast("NFC Not Supported!")
            liveTag.emit("NFC Not Supported!")
        }
        if (NFCManager.isSupportedAndEnabled(getApplication()) && status == NFCStatus.Tap) {
            livePayment.emit("Put your card against device to process payment")
        } else {
            liveTag.emit(null)
        }
    }

    /**
     * Observes NFC status changes.
     *
     * @return StateFlow of NFCStatus.
     */
     fun observeNFCStatus(): StateFlow<NFCStatus?> {
        return liveNFC.asStateFlow()
    }

    //endregion
    //region Tags Information Methods
    private fun getDateTimeNow(): String {
        Log.d(TAG, "getDateTimeNow()")
        val TIME_FORMAT: DateFormat = SimpleDateFormat.getDateTimeInstance()
        val now: Date = Date()
        Log.d(ContentValues.TAG, "getDateTimeNow() Return ${TIME_FORMAT.format(now)}")
        return TIME_FORMAT.format(now)
    }

    /**
     * Converts a ByteArray to a hexadecimal String representation.
     * Each byte is converted to a two-digit hex number, resulting in a string
     * where each pair of characters represents one byte of the array.
     *
     * @param bytes The ByteArray to be converted.
     * @return A String containing the hexadecimal representation of the input bytes.
     */
    private fun getHex(bytes: ByteArray): String {
        val sb = StringBuilder()
        for (i in bytes.indices.reversed()) {
            val b: Int = bytes[i].toInt() and 0xFF // Mask to get the lowest 8 bits
            if (b < 0x10) sb.append('0')
            sb.append(Integer.toHexString(b))
        }
        return sb.toString()
    }

    /**
     * Converts a ByteArray to its decimal representation. This method treats the byte array
     * as a little-endian sequence of bytes, where the first byte is the least significant.
     *
     * @param bytes The ByteArray to be converted.
     * @return The long value representing the decimal equivalent of the byte array.
     */
    private fun getDec(bytes: ByteArray): Long {
        Log.d(TAG, "getDec()")
        var result: Long = 0
        var factor: Long = 1
        for (i in bytes.indices) {
            val value: Long = bytes[i].and(0xffL.toByte()).toLong()
            result += value * factor
            factor *= 256L
        }
        return result
    }

    /**
     * Converts a ByteArray to its decimal representation in reverse order.
     * This method treats the byte array as a big-endian sequence of bytes, where the
     * last byte is the least significant.
     *
     * @param bytes The ByteArray to be converted.
     * @return The long value representing the decimal equivalent of the reversed byte array.
     */
    private fun getReversed(bytes: ByteArray): Long {
        Log.d(TAG, "getReversed()")
        var result: Long = 0
        var factor: Long = 1
        for (i in bytes.indices.reversed()) {
            val value = bytes[i].and(0xffL.toByte()).toLong()
            result += value * factor
            factor *= 256L
        }
        return result
    }

     fun observeLivePayment(): StateFlow<String?> {
        return livePayment.asStateFlow()
    }

    /**
     * Sets the status of payment success.
     *
     * @param status Status of the payment.
     */
    fun setPaymentSuccess(status: Boolean) {
        isPaymentSuccessful = status
    }

    /**
     * Releases the MediaPlayer resources when the ViewModel is destroyed.
     */
    fun onDestroy() {
        mediaPlayer?.release()
        mediaPlayer = null
    }
    //endregion
}