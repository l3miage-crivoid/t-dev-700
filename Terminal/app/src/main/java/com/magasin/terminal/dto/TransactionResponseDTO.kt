package com.magasin.terminal.dto

data class TransactionResponseDTO(
    val message: String,
    val amountToSubstract: Double
)
