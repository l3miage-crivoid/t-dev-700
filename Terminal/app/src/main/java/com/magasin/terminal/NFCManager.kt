package com.magasin.terminal

import android.app.Activity
import android.content.Context
import android.nfc.NfcAdapter
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.annotation.RequiresApi

/**
 * Utility class for managing NFC operations in Android applications.
 */
public object NFCManager {

    private val TAG = NFCManager::class.java.getSimpleName()

    /**
     * Enables NFC reader mode for the given activity.
     *
     * @param context The application context.
     * @param activity The activity on which the NFC reader mode is to be enabled.
     * @param callback A callback that will be triggered when an NFC tag is discovered.
     * @param flags Flags to configure the NFC reader mode.
     * @param extras Additional extras for NFC reader mode configuration.
     */
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun enableReaderMode(context : Context, activity : Activity, callback : NfcAdapter.ReaderCallback, flags : Int, extras : Bundle) {
        try {
            NfcAdapter.getDefaultAdapter(context).enableReaderMode(activity, callback, flags, extras)
        } catch (ex : UnsupportedOperationException) {
            Log.e(TAG,"UnsupportedOperationException ${ex.message}", ex)
        }
    }

    /**
     * Disables NFC reader mode for the given activity.
     *
     * @param context The application context.
     * @param activity The activity on which the NFC reader mode is to be disabled.
     */
    @RequiresApi(Build.VERSION_CODES.KITKAT)
    fun disableReaderMode(context : Context, activity : Activity) {
        try {
            NfcAdapter.getDefaultAdapter(context).disableReaderMode(activity)
        } catch (ex : UnsupportedOperationException) {
            Log.e(TAG,"UnsupportedOperationException ${ex.message}", ex)
        }
    }

    /**
     * Checks if NFC is supported on the device.
     *
     * @param context The application context.
     * @return True if NFC is supported, false otherwise.
     */
    fun isSupported(context : Context) : Boolean {
        val nfcAdapter = NfcAdapter.getDefaultAdapter(context)
        return nfcAdapter != null
    }

    /**
     * Checks if NFC is not supported on the device.
     *
     * @param context The application context.
     * @return True if NFC is not supported, false otherwise.
     */
    fun isNotSupported(context : Context) : Boolean {
        val nfcAdapter = NfcAdapter.getDefaultAdapter(context)
        return nfcAdapter == null
    }

    /**
     * Checks if NFC is enabled on the device.
     *
     * @param context The application context.
     * @return True if NFC is enabled, false otherwise.
     */
    fun isEnabled(context : Context) : Boolean {
        val nfcAdapter = NfcAdapter.getDefaultAdapter(context)
        return nfcAdapter?.isEnabled ?: false
    }

    /**
     * Checks if NFC is not enabled on the device.
     *
     * @param context The application context.
     * @return True if NFC is not enabled, false otherwise.
     */
    fun isNotEnabled(context : Context) : Boolean {
        val nfcAdapter = NfcAdapter.getDefaultAdapter(context)
        return nfcAdapter?.isEnabled?.not() ?: true
    }

    /**
     * Checks if NFC is both supported and enabled on the device.
     *
     * @param context The application context.
     * @return True if both conditions are met, false otherwise.
     */
    fun isSupportedAndEnabled(context : Context) : Boolean {
        return isSupported(context) && isEnabled(context)
    }
}