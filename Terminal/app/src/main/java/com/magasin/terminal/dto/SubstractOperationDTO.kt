package com.magasin.terminal.dto

data class SubstractOperationDTO(
    var uid: String? = null,
    var amountToSubstract: Double? = null
)
