package com.magasin.terminal
import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.MutableLiveData
import com.magasin.terminal.dto.Stock
import com.magasin.terminal.dto.StockList

class CartViewModel : ViewModel() {

    val cartItems: MutableLiveData<StockList> = MutableLiveData()
    val selectedItemsCount: MutableLiveData<Int> = MutableLiveData(0)

    private val _cartTotal = MutableLiveData<Double>()
    val cartTotal: LiveData<Double> get() = _cartTotal

    // Add item to the cart
    fun addToCart(stock: Stock) {
        selectedItemsCount.value = selectedItemsCount.value?.plus(1)
        _cartTotal.value = (_cartTotal.value ?: 0.0) + (stock.price ?: 0.0)
    }

    // Remove item from the cart
    fun removeFromCart(cartItem: Stock) {
        selectedItemsCount.value = selectedItemsCount.value?.minus(1)
        _cartTotal.value = (_cartTotal.value ?: 0.0) - (cartItem.price ?: 0.0)
    }

    fun clearCart() {
        selectedItemsCount.value = 0
        _cartTotal.value = 0.0
    }
}


