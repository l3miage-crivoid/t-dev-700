package com.magasin.terminal.dto
data class
Stock(
    var id: Int? = null,
    var name: String? = null,
    var price: Double? = null,
)
