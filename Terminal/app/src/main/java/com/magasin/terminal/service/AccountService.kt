package com.magasin.terminal.service

import com.magasin.terminal.dto.JwtResponseDTO
import com.magasin.terminal.dto.LoginDTO
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.POST

interface AccountService {
    @POST("/api/account/sign-in")
    suspend fun loginUser(@Body loginDTO: LoginDTO): Response<JwtResponseDTO>
}