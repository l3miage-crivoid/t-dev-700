package com.magasin.terminal;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;
import com.magasin.terminal.databinding.ItemStockBinding;
import com.magasin.terminal.dto.Stock;
import com.magasin.terminal.dto.StockList;

class StockListAdapter: RecyclerView.Adapter<StockListAdapter.StockViewHolder>() {

    // Variable to hold the binding for the individual list item
    lateinit var binding: ItemStockBinding;

    // A variable to hold the list of stock items
    var stockList: StockList = StockList(emptyList());

    // A listener for item click events
    private var onItemClickListener: ((Stock) -> Unit)? = null;

    // Method to set the item click listener
    fun setOnItemClickListener(listener: (Stock) -> Unit) {
        this.onItemClickListener = listener;
    }

    // Method to create new ViewHolder instances
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StockViewHolder {
        val inflater = LayoutInflater.from(parent.context);
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.item_stock,
            parent,
            false
        );
        return StockViewHolder(binding);
    }

    // Method to update the list of stock items and refresh the RecyclerView
    fun setStock(newStocks: StockList) {
        stockList = newStocks;
        notifyDataSetChanged();
    }

    // Bind the data to each list item
    override fun onBindViewHolder(holder: StockViewHolder, position: Int) {
        stockList?.stocks?.get(position)?.let { stock ->
            holder.bind(stock);
            holder.itemView.setOnClickListener {
                onItemClickListener?.invoke(stock);
            }
        };
    }

    // Return the size of the stock list
    override fun getItemCount(): Int = stockList?.stocks?.size!!;

    // Define ViewHolder inner class for managing list item views
    inner class StockViewHolder(private val binding: ItemStockBinding) :
        RecyclerView.ViewHolder(binding.root) {

        // Bind a stock item to this ViewHolder
        fun bind(stock: Stock) {
            binding.stock = stock
            binding.executePendingBindings()
        }
    }
}
