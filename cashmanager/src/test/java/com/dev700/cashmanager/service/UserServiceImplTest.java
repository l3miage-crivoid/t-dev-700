package com.dev700.cashmanager.service;

import com.dev700.cashmanager.dto.UserDTO;
import com.dev700.cashmanager.entity.UserEntity;
import com.dev700.cashmanager.repository.UserRepository;
import com.dev700.cashmanager.dto.mapper.UserDTOMapper;
import com.dev700.cashmanager.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class UserServiceImplTest {

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserDTOMapper userDTOMapper;

    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        userService = new UserServiceImpl(userRepository, userDTOMapper);
    }

    @Test
    public void testGetUsers() {
        // Setup
        UserEntity userEntity1 = new UserEntity();
        UserEntity userEntity2 = new UserEntity();
        List<UserEntity> userEntities = Arrays.asList(userEntity1, userEntity2);
        when(userRepository.getUsers()).thenReturn(userEntities);

        // Mock the DTOs returned by the mapper
        UserDTO userDTO1 = new UserDTO();
        UserDTO userDTO2 = new UserDTO();
        List<UserDTO> userDTOs = Arrays.asList(userDTO1, userDTO2);
        when(userDTOMapper.userEntityToUserDTO(userEntities)).thenReturn(userDTOs);

        // Execute
        List<UserDTO> result = userService.getUsers();

        // Verify
        verify(userRepository).getUsers();
        verify(userDTOMapper).userEntityToUserDTO(userEntities);
        assertEquals(2, result.size());
    }
}
