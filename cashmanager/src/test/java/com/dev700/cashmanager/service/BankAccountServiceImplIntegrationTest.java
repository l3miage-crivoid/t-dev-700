package com.dev700.cashmanager.service;

import com.dev700.cashmanager.entity.BankAccountEntity;
import com.dev700.cashmanager.exception.BankAccountNotFoundException;
import com.dev700.cashmanager.repository.BankAccountRepository;
import com.dev700.cashmanager.service.impl.BankAccountServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@SpringBootTest
public class BankAccountServiceImplIntegrationTest {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    private BankAccountService bankAccountService;

    @BeforeEach
    void setUp() {
        // Mock the BankAccountRepository
        bankAccountRepository = mock(BankAccountRepository.class);
        bankAccountService = new BankAccountServiceImpl(bankAccountRepository);
    }

    @Test
    public void testGetBankAccountByUidFound() {
        // Setup
        String uid = "testUid";
        BankAccountEntity bankAccount = new BankAccountEntity();
        bankAccount.setUid(uid);
        when(bankAccountRepository.getBankAccountByUid(uid)).thenReturn(bankAccount);

        // Execute
        BankAccountEntity result = bankAccountService.getBankAccountByUid(uid);

        // Verify
        assertEquals(uid, result.getUid());
        verify(bankAccountRepository).getBankAccountByUid(uid);
    }

    @Test
    public void testGetBankAccountByUidNotFound() {
        // Setup
        String uid = "unknownUid";
        when(bankAccountRepository.getBankAccountByUid(uid)).thenReturn(null);

        // Execute and Verify
        assertThrows(BankAccountNotFoundException.class, () -> {
            bankAccountService.getBankAccountByUid(uid);
        });
    }

    @Test
    public void testSubstractAmount() {
        // Setup
        BankAccountEntity bankAccount = new BankAccountEntity();
        bankAccount.setAmountOfMoney(100.0);
        double amountToSubstract = 50.0;

        // Execute
        Double result = bankAccountService.substractAmount(amountToSubstract, bankAccount);

        // Verify
        assertEquals(50.0, bankAccount.getAmountOfMoney());
        assertEquals(amountToSubstract, result);
        verify(bankAccountRepository).upsertBankAccountByID(bankAccount);
    }

    @Test
    public void testSubstractAmountInsufficientFunds() {
        // Setup
        BankAccountEntity bankAccount = new BankAccountEntity();
        bankAccount.setAmountOfMoney(30.0);
        double amountToSubstract = 50.0;

        // Execute
        Double result = bankAccountService.substractAmount(amountToSubstract, bankAccount);

        // Verify
        assertNull(result);
        verify(bankAccountRepository, never()).upsertBankAccountByID(any(BankAccountEntity.class));
    }
}
