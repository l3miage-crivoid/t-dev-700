package com.dev700.cashmanager.service;

import com.dev700.cashmanager.dto.StockDTO;
import com.dev700.cashmanager.entity.StockEntity;
import com.dev700.cashmanager.repository.StockRepository;
import com.dev700.cashmanager.dto.mapper.StockDTOMapper;
import com.dev700.cashmanager.service.impl.StockServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@SpringBootTest
public class StockServiceImplTest {

    @Mock
    private StockRepository stockRepository;

    @Mock
    private StockDTOMapper stockDTOMapper;

    private StockService stockService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
        stockService = new StockServiceImpl(stockRepository, stockDTOMapper);
    }

    @Test
    public void testGetStock() {
        // Setup
        StockEntity stockEntity1 = new StockEntity(); // Assume StockEntity is your entity class
        StockEntity stockEntity2 = new StockEntity();
        List<StockEntity> stockEntities = Arrays.asList(stockEntity1, stockEntity2);
        when(stockRepository.getStock()).thenReturn(stockEntities);

        StockDTO stockDTO1 = new StockDTO();
        StockDTO stockDTO2 = new StockDTO();
        List<StockDTO> stockDTOs = Arrays.asList(stockDTO1, stockDTO2);
        when(stockDTOMapper.stockEntitiesToStockDTO(stockEntities)).thenReturn(stockDTOs);

        // Execute
        List<StockDTO> result = stockService.getStock();

        // Verify
        verify(stockRepository).getStock();
        verify(stockDTOMapper).stockEntitiesToStockDTO(stockEntities);
        assertEquals(2, result.size());
    }
}
