package com.dev700.cashmanager;

import com.dev700.cashmanager.jooq.context.VmDSLContext;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.test.context.TestPropertySource;

@Import(value = {TestsConfig.class})
@TestPropertySource(locations= "classpath:application.yml")
@SpringBootTest
public class TestJooq {
    @Autowired
    protected VmDSLContext dsl;

    @BeforeEach
    public void initTables() {
        dsl.meta()
                .getTables()
                .forEach(table -> dsl.truncate(table).cascade().execute());
    }

    @AfterEach
    public void truncateTables() {
        dsl.meta()
                .getTables()
                .forEach(table -> dsl.truncate(table).cascade().execute());
    }
}

