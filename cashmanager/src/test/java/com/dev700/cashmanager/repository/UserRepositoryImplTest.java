package com.dev700.cashmanager.repository;

import static com.dev700.cashmanager.jooq.generate.table.tables.User.USER;
import static org.junit.jupiter.api.Assertions.*;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.dev700.cashmanager.TestJooq;
import com.dev700.cashmanager.entity.UserEntity;


public class UserRepositoryImplTest extends TestJooq {

    @Autowired
    private UserRepository userRepository;

    @Test
    public void testInsertAndSelectUsers() {
        // GIVEN
        dsl.insertInto(USER)
                .set(USER.USERNAME, "testuser1")
                .set(USER.PASSWORD, "password1")
                .execute();

        dsl.insertInto(USER)
                .set(USER.USERNAME, "testuser2")
                .set(USER.PASSWORD, "password2")
                .execute();
        // WHEN
        List<UserEntity> liste = userRepository.getUsers();
        // THEN
        assertEquals(liste.size(), 2);
        assertEquals(liste.get(0).getUsername(), "testuser1");
        assertEquals(liste.get(0).getPassword(), "password1");
        
        assertEquals(liste.get(1).getPassword(), "password2");
        assertEquals(liste.get(1).getUsername(), "testuser2");
    }

    @Test
    public void getUserByUsernameTest() {
        // GIVEN: Insert a user in the database
        userRepository.upsertUser(new UserEntity("testuser" , "testpass"));

        // WHEN: Retrieve user by username
        UserEntity retrievedUser = userRepository.getUserByUsername("testuser");

        // THEN: Check if the retrieved user is as expected
        assertNotNull(retrievedUser, "User should not be null");
        assertEquals("testuser", retrievedUser.getUsername(), "Username should match");

        // Test for non-existing user
        UserEntity nonExistentUser = userRepository.getUserByUsername("nonexistent");
        assertNull(nonExistentUser, "Non-existent user should return null");
    }
}
