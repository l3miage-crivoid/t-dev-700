package com.dev700.cashmanager.repository;

import static com.dev700.cashmanager.jooq.generate.table.tables.BankAccount.BANK_ACCOUNT;

import com.dev700.cashmanager.TestJooq;
import com.dev700.cashmanager.entity.BankAccountEntity;
import com.dev700.cashmanager.repository.impl.BankAccountRepositoryImpl;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import static org.junit.jupiter.api.Assertions.*;

public class BankAccountRepositoryImplTest extends TestJooq {

    @Autowired
    private BankAccountRepositoryImpl bankAccountRepository;

    @Test
    public void testGetBankAccountByUid() {
        // GIVEN
        dsl.insertInto(BANK_ACCOUNT)
                .set(BANK_ACCOUNT.UID, "testuid")
                .set(BANK_ACCOUNT.EMAIL, "test@example.com")
                .set(BANK_ACCOUNT.AMOUNT_OF_MONEY, 100.0)
                .execute();

        // WHEN
        BankAccountEntity result = bankAccountRepository.getBankAccountByUid("testuid");

        // THEN
        assertEquals("testuid", result.getUid());
        assertEquals("test@example.com", result.getEmail());
        assertEquals(100.0, result.getAmountOfMoney());
    }

    @Test
    public void testUpsertBankAccountByID() {
        // THEN
        BankAccountEntity bankAccountEntity = new BankAccountEntity();
        bankAccountEntity.setUid("upsertuid");
        bankAccountEntity.setEmail("upsert@example.com");
        bankAccountEntity.setAmountOfMoney(200.0);

        // WHEN
        bankAccountRepository.upsertBankAccountByID(bankAccountEntity);

        // Retrieve the upserted record
        BankAccountEntity result = dsl.selectFrom(BANK_ACCOUNT)
                .where(BANK_ACCOUNT.UID.eq("upsertuid"))
                .fetchOneInto(BankAccountEntity.class);

        // THEN
        assertNotNull(result);
        assertEquals("upsertuid", result.getUid());
        assertEquals("upsert@example.com", result.getEmail());
        assertEquals(200.0, result.getAmountOfMoney());
    }
}
