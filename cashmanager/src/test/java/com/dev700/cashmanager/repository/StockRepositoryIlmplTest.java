package com.dev700.cashmanager.repository;


import static com.dev700.cashmanager.jooq.generate.table.tables.Stock.STOCK;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.dev700.cashmanager.TestJooq;
import com.dev700.cashmanager.entity.StockEntity;


public class StockRepositoryIlmplTest extends TestJooq {
    
    @Autowired

    private StockRepository stockRepository;
    @Test
    public void testInsertAndSelectUsers() {
        // GIVEN
        dsl.insertInto(STOCK)
                .set(STOCK.NAME, "stockname1")
                .set(STOCK.QUANTITY, 11)
                .execute();

        dsl.insertInto(STOCK)
                .set(STOCK.NAME, "stockname2")
                .set(STOCK.QUANTITY, 89)
                .execute();
        // WHEN
        List<StockEntity> liste = stockRepository.getStock();
        // THEN
        assertEquals(liste.size(), 2);
        assertEquals(liste.get(0).getName(), "stockname1");
        assertEquals(liste.get(0).getQuantity(), 11);
        
        assertEquals(liste.get(1).getName(), "stockname2");
        assertEquals(liste.get(1).getQuantity(), 89);


    }
    
    
}
