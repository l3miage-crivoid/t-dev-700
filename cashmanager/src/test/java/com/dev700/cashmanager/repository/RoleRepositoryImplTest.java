package com.dev700.cashmanager.repository;

import com.dev700.cashmanager.TestJooq;
import com.dev700.cashmanager.entity.RoleEntity;
import com.dev700.cashmanager.entity.UserEntity;
import com.dev700.cashmanager.repository.enumeration.RoleEnum;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RoleRepositoryImplTest  extends TestJooq {
    @Autowired
    private RoleRepository roleRepository;
    @Autowired
    private UserRepository userRepository;

    @Test
    public void retrieveUserRoleOk(){
        // GIVEN
        UserEntity user = new UserEntity();
        user.setUsername("DMITRII");
        user.setPassword("default");
        Long userId = userRepository.upsertUser(user);

        roleRepository.upsertRole(new RoleEntity(userId, RoleEnum.ROLE_ADMIN));
        roleRepository.upsertRole(new RoleEntity(userId, RoleEnum.ROLE_USER));

        // WHEN
        List<RoleEntity> roles = roleRepository.getRolesByUserId(userId);

        //THEN
        assertEquals(RoleEnum.ROLE_ADMIN, roles.get(0).getRoleName());
        assertEquals(RoleEnum.ROLE_USER, roles.get(1).getRoleName());
    }
}
