package com.dev700.cashmanager.mapper;

import com.dev700.cashmanager.entity.StockEntity;
import com.dev700.cashmanager.jooq.generate.table.tables.records.StockRecord;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface StockMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "name", source = "name")
    @Mapping(target = "quantity", source = "quantity")
    @Mapping(target = "price", source = "price")
    StockEntity recordToEntity(StockRecord stockRecord);

    @InheritInverseConfiguration
    StockRecord entityToRecord(StockEntity stockEntity);
}
