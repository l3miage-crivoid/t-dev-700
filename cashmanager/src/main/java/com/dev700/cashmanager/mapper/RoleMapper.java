package com.dev700.cashmanager.mapper;

import com.dev700.cashmanager.entity.RoleEntity;
import com.dev700.cashmanager.jooq.generate.table.tables.records.RolesRecord;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface RoleMapper {
    @Mapping(target = "roleId", source = "id")
    @Mapping(target = "userId", source = "userId")
    @Mapping(target = "roleName", source = "roleName")
    RolesRecord entityToRecord(RoleEntity roleEntity);

    @InheritInverseConfiguration
    RoleEntity recordToEntity(RolesRecord rolesRecord);
}
