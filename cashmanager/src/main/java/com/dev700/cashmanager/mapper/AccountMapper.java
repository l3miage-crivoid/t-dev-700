package com.dev700.cashmanager.mapper;

import com.dev700.cashmanager.entity.BankAccountEntity;
import com.dev700.cashmanager.jooq.generate.table.tables.records.BankAccountRecord;
import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.NullValueCheckStrategy;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface AccountMapper {
    @Mapping(target = "id", source = "id")
    @Mapping(target = "uid", source = "uid")
    @Mapping(target = "email", source = "email")
    @Mapping(target = "amountOfMoney", source = "amountOfMoney")
    BankAccountEntity recordToEntity(BankAccountRecord accountEntity);

    @InheritInverseConfiguration
    BankAccountRecord entityToRecord(BankAccountEntity bankAccountEntity);
}
