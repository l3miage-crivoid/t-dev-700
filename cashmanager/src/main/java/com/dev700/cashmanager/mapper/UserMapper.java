package com.dev700.cashmanager.mapper;

import com.dev700.cashmanager.entity.UserEntity;
import com.dev700.cashmanager.jooq.generate.table.tables.records.UserRecord;
import org.mapstruct.*;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface UserMapper {
    @Mapping(target = "userId", source = "userId")
    @Mapping(target = "username", source = "username")
    @Mapping(target = "password", source = "password")
    UserEntity recordToEntity(UserRecord userRecord);

    @InheritInverseConfiguration
    UserRecord entityToRecord(UserEntity userEntity);
}
