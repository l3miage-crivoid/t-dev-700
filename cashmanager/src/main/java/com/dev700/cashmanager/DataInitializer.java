package com.dev700.cashmanager;

import com.dev700.cashmanager.entity.RoleEntity;
import com.dev700.cashmanager.entity.UserEntity;
import com.dev700.cashmanager.jooq.generate.table.enums.Role;
import com.dev700.cashmanager.repository.enumeration.RoleEnum;
import com.dev700.cashmanager.repository.impl.RoleRepositoryImpl;
import com.dev700.cashmanager.repository.impl.UserRepositoryImpl;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class DataInitializer {

    @Bean
    CommandLineRunner init(UserRepositoryImpl userRepository, RoleRepositoryImpl roleRepository, PasswordEncoder passwordEncoder) {
        return args -> {
            // Check if user already exists to avoid duplicate creation
            UserEntity existingUser = userRepository.getUserByUsername("DMITRII");
            UserEntity withRoleUser = userRepository.getUserByUsername("DMITRII_USER");
            if (existingUser == null) {
                UserEntity user = new UserEntity();
                user.setUsername("DMITRII");
                user.setPassword(passwordEncoder.encode("defaultPassword"));
                Long userId = userRepository.upsertUser(user);
                // Set other user properties and roles
                roleRepository.upsertRole(new RoleEntity(userId, RoleEnum.ROLE_ADMIN));
                roleRepository.upsertRole(new RoleEntity(userId, RoleEnum.ROLE_USER));
            }
            if(withRoleUser == null){
                UserEntity user = new UserEntity();
                user.setUsername("DMITRII_USER");
                user.setPassword(passwordEncoder.encode("defaultPasswordUser"));
                Long userId = userRepository.upsertUser(user);
                // Set other user properties and roles
                roleRepository.upsertRole(new RoleEntity(userId, RoleEnum.ROLE_USER));
            }
        };
    }
}
