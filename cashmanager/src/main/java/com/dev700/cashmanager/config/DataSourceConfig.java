package com.dev700.cashmanager.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;

// Annotate the class as a configuration class. This class is used for Spring's Java-based configuration.
@Configuration
public class DataSourceConfig {
    // Injects the database URL from the application's properties file.
    @Value("${spring.datasource.url}")
    private String databaseUrl;

    // Injects the database username from the application's properties file.
    @Value("${spring.datasource.username}")
    private String databaseUsername;

    // Injects the database password from the application's properties file.
    @Value("${spring.datasource.password}")
    private String databasePassword;

    // Injects the database driver class name from the application's properties file.
    @Value("${spring.datasource.driver-class-name}")
    private String driverClassName;

    // Declares a bean for the DataSource which is used to manage the database connection.
    @Bean
    public DataSource dataSource() {
        // Creates a new instance of DriverManagerDataSource, which is an implementation of DataSource.
        DriverManagerDataSource dataSource = new DriverManagerDataSource();

        // Sets the JDBC driver class name.
        dataSource.setDriverClassName(driverClassName);

        // Sets the database URL.
        dataSource.setUrl(databaseUrl);

        // Sets the database username.
        dataSource.setUsername(databaseUsername);

        // Sets the database password.
        dataSource.setPassword(databasePassword);

        // Returns the configured data source.
        return dataSource;
    }
}
