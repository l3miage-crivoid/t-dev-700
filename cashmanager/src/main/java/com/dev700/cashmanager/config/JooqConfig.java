package com.dev700.cashmanager.config;

import com.dev700.cashmanager.jooq.context.VmDSLContext;
import org.jooq.SQLDialect;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;

@Configuration
public class JooqConfig {

    @Bean
    public VmDSLContext vmDSLContext(DataSource dataSource) {
        return new VmDSLContext(dataSource, SQLDialect.POSTGRES);
    }

}
