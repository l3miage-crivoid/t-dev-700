package com.dev700.cashmanager.jooq.context;

import org.jooq.Record;
import org.jooq.*;
import org.jooq.conf.Settings;
import org.jooq.impl.DefaultDSLContext;
import org.jooq.impl.TableImpl;

import javax.sql.DataSource;
import java.sql.Connection;
import java.util.Arrays;
import java.util.Collection;
import java.util.Map;

public class VmDSLContext extends DefaultDSLContext {
    /**
     * Constructor initializing with SQL dialect.
     * @param dialect The SQL dialect to be used.
     */
    public VmDSLContext(SQLDialect dialect) {
        super(dialect);
    }

    /**
     * Constructor initializing with SQL dialect and settings.
     * @param dialect The SQL dialect to be used.
     * @param settings jOOQ settings.
     */
    public VmDSLContext(SQLDialect dialect, Settings settings) {
        super(dialect, settings);
    }

    /**
     * Constructor initializing with a database connection and SQL dialect.
     * @param connection Database connection.
     * @param dialect The SQL dialect to be used.
     */
    public VmDSLContext(Connection connection, SQLDialect dialect) {
        super(connection, dialect);
    }

    /**
     * Constructor initializing with a database connection, SQL dialect, and settings.
     * @param connection Database connection.
     * @param dialect The SQL dialect to be used.
     * @param settings jOOQ settings.
     */
    public VmDSLContext(Connection connection, SQLDialect dialect, Settings settings) {
        super(connection, dialect, settings);
    }

    /**
     * Constructor initializing with a data source and SQL dialect.
     * @param datasource Data source for database connections.
     * @param dialect The SQL dialect to be used.
     */
    public VmDSLContext(DataSource datasource, SQLDialect dialect) {
        super(datasource, dialect);
    }

    /**
     * Constructor initializing with a data source, SQL dialect, and settings.
     * @param datasource Data source for database connections.
     * @param dialect The SQL dialect to be used.
     * @param settings jOOQ settings.
     */
    public VmDSLContext(DataSource datasource, SQLDialect dialect, Settings settings) {
        super(datasource, dialect, settings);
    }

    /**
     * Constructor initializing with a connection provider and SQL dialect.
     * @param connectionProvider Provider for database connections.
     * @param dialect The SQL dialect to be used.
     */
    public VmDSLContext(ConnectionProvider connectionProvider, SQLDialect dialect) {
        super(connectionProvider, dialect);
    }

    /**
     * Constructor initializing with a connection provider, SQL dialect, and settings.
     * @param connectionProvider Provider for database connections.
     * @param dialect The SQL dialect to be used.
     * @param settings jOOQ settings.
     */
    public VmDSLContext(ConnectionProvider connectionProvider, SQLDialect dialect, Settings settings) {
        super(connectionProvider, dialect, settings);
    }

    /**
     * Constructor initializing with a jOOQ configuration.
     * @param configuration jOOQ configuration.
     */
    public VmDSLContext(Configuration configuration) {
        super(configuration);
    }

    /**
     * Custom method to perform an 'upsert' operation on a given table.
     * @param <T> The record type extending jOOQ Record.
     * @param table The table on which to perform the upsert.
     * @param record The record to insert or update.
     * @param id The field representing the ID, used for returning the result.
     * @return The ID of the inserted/updated record.
     */
    public <T extends Record> Long upsert(TableImpl<T> table, T record, TableField<T, Long> id) {
        return this.insertInto(table)
                .set(record)
                .onDuplicateKeyUpdate()
                .set(record)
                .returningResult(id)
                .fetchOne()
                .into(long.class);
    }
}

