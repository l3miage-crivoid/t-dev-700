package com.dev700.cashmanager.service;

import com.dev700.cashmanager.dto.StockDTO;

import java.util.List;

public interface StockService {
    List<StockDTO> getStock();
}
