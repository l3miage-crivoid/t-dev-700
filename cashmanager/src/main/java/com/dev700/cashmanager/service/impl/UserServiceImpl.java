package com.dev700.cashmanager.service.impl;

import com.dev700.cashmanager.entity.UserEntity;
import com.dev700.cashmanager.repository.UserRepository;
import com.dev700.cashmanager.dto.UserDTO;
import com.dev700.cashmanager.dto.mapper.UserDTOMapper;
import com.dev700.cashmanager.service.UserService;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService , UserDetailsService {
    private final UserRepository userRepository;
    private final UserDTOMapper userDTOMapper;

    public UserServiceImpl(UserRepository userRepository, UserDTOMapper userDTOMapper) {
        this.userRepository = userRepository;
        this.userDTOMapper = userDTOMapper;
    }

    @Override
    public List<UserDTO> getUsers() {
        return userDTOMapper.userEntityToUserDTO(userRepository.getUsers());
    }

    @Override
    public UserEntity getUserByUsername(String username) {
        return userRepository.getUserByUsername(username);
    }

    @Override
    public UserEntity loadUserByUsername(String username) throws UsernameNotFoundException {
        return userRepository.getUserByUsername(username);
    }
}
