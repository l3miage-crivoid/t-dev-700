package com.dev700.cashmanager.service;

import com.dev700.cashmanager.entity.BankAccountEntity;

public interface BankAccountService {
    /**
     * Retrieves a BankAccountEntity based on its unique identifier (UID).
     *
     * @param uid The unique identifier for the bank account.
     * @return BankAccountEntity associated with the UID. Returns null if no account is found.
     */
    BankAccountEntity getBankAccountByUid(String uid);

    /**
     * Subtracts a specified amount from the balance of a bank account.
     *
     * @param amountToSubstract The amount to be subtracted from the account's balance.
     * @param bankAccountEntity The bank account from which the amount will be subtracted.
     * @return Double representing the new balance of the account. Null or exception on failure.
     */
    Double substractAmount(Double amountToSubstract, BankAccountEntity bankAccountEntity);
}
