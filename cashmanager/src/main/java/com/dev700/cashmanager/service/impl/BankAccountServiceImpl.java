package com.dev700.cashmanager.service.impl;

import com.dev700.cashmanager.exception.BankAccountNotFoundException;
import com.dev700.cashmanager.entity.BankAccountEntity;
import com.dev700.cashmanager.repository.BankAccountRepository;
import com.dev700.cashmanager.service.BankAccountService;
import org.springframework.stereotype.Service;

@Service
public class BankAccountServiceImpl implements BankAccountService {
    private final BankAccountRepository bankAccountRepository;

    public BankAccountServiceImpl(BankAccountRepository bankAccountRepository) {
        this.bankAccountRepository = bankAccountRepository;
    }

    @Override
    public BankAccountEntity getBankAccountByUid(String uid) {
        BankAccountEntity bankAccount = bankAccountRepository.getBankAccountByUid(uid);

        if (bankAccount == null) {
            throw new BankAccountNotFoundException("User not found in bank");
        }

        return bankAccount;
    }

    @Override
    public Double substractAmount(Double amountToSubstract, BankAccountEntity bankAccountEntity) {
        if(amountToSubstract <= bankAccountEntity.getAmountOfMoney()){
            bankAccountEntity.setAmountOfMoney( bankAccountEntity.getAmountOfMoney() - amountToSubstract );
            bankAccountRepository.upsertBankAccountByID(bankAccountEntity);
            return amountToSubstract;
        }
        return null;
    }
}
