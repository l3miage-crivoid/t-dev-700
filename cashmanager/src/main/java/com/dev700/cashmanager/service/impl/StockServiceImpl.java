package com.dev700.cashmanager.service.impl;

import com.dev700.cashmanager.repository.StockRepository;
import com.dev700.cashmanager.dto.StockDTO;
import com.dev700.cashmanager.dto.mapper.StockDTOMapper;
import com.dev700.cashmanager.service.StockService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StockServiceImpl implements StockService {
    private final StockRepository stockRepository;
    private final StockDTOMapper stockDTOMapper;

    public StockServiceImpl(StockRepository stockRepository, StockDTOMapper stockDTOMapper) {
        this.stockRepository = stockRepository;
        this.stockDTOMapper = stockDTOMapper;
    }

    @Override
    public List<StockDTO> getStock() {
        return stockDTOMapper.stockEntitiesToStockDTO(stockRepository.getStock());
    }
}
