package com.dev700.cashmanager.repository.impl;

import com.dev700.cashmanager.jooq.generate.table.enums.Role;
import com.dev700.cashmanager.repository.UserRepository;
import com.dev700.cashmanager.jooq.context.VmDSLContext;
import com.dev700.cashmanager.jooq.generate.table.tables.records.UserRecord;
import com.dev700.cashmanager.entity.UserEntity;
import com.dev700.cashmanager.mapper.UserMapper;
import com.dev700.cashmanager.repository.enumeration.RoleEnum;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.stream.Collectors;

import static com.dev700.cashmanager.jooq.generate.table.tables.BankAccount.BANK_ACCOUNT;
import static com.dev700.cashmanager.jooq.generate.table.tables.User.USER;
import static com.dev700.cashmanager.jooq.generate.table.tables.Roles.ROLES;

@Repository
public class UserRepositoryImpl implements UserRepository {
    private final VmDSLContext dslContext;
    private final UserMapper userMapper;

    public UserRepositoryImpl(VmDSLContext dslContext, UserMapper userMapper) {
        this.dslContext = dslContext;
        this.userMapper = userMapper;
    }

    @Override
    public List<UserEntity> getUsers() {
        return dslContext
                .selectFrom(USER)
                .fetch(record -> userMapper.recordToEntity(record.into(UserRecord.class)));
    }

    @Override
    public UserEntity getUserByUsername(String username) {
        UserEntity user = dslContext
                .select(USER.fields())
                .from(USER)
                .where(USER.USERNAME.eq(username))
                .fetchOne(record -> userMapper.recordToEntity(record.into(UserRecord.class)));

        if (user != null) {
            List<Role> roles = dslContext.select(ROLES.ROLE_NAME)
                    .from(ROLES)
                    .where(ROLES.USER_ID.eq(user.getUserId()))
                    .fetch(record -> record.into(Role.class));

            user.setAuthorities(roles.stream().map(RoleEnum::convert)
                    .collect(Collectors.toSet()));
        }
        return user;
    }

    @Override
    public Long upsertUser(UserEntity userEntity) {
        return dslContext.upsert(USER, userMapper.entityToRecord(userEntity), USER.USER_ID).longValue();
    }
}
