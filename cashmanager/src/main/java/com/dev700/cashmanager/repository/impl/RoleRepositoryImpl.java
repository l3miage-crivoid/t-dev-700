package com.dev700.cashmanager.repository.impl;

import com.dev700.cashmanager.entity.RoleEntity;
import com.dev700.cashmanager.jooq.context.VmDSLContext;
import com.dev700.cashmanager.jooq.generate.table.tables.records.RolesRecord;
import com.dev700.cashmanager.mapper.RoleMapper;
import com.dev700.cashmanager.repository.RoleRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.dev700.cashmanager.jooq.generate.table.Tables.ROLES;

@Repository
public class RoleRepositoryImpl implements RoleRepository {
    private final VmDSLContext dslContext;
    private final RoleMapper roleMapper;

    public RoleRepositoryImpl(VmDSLContext dslContext, RoleMapper roleMapper) {
        this.dslContext = dslContext;
        this.roleMapper = roleMapper;
    }

    @Override
    public List<RoleEntity> getRolesByUserId(Long userId){
        return dslContext.selectFrom(ROLES)
                .where(ROLES.USER_ID.eq(userId))
                .fetch(record -> roleMapper.recordToEntity(record.into(RolesRecord.class)));
    }

    @Override
    public void upsertRole(RoleEntity roleEntity) {
        dslContext.upsert(ROLES, roleMapper.entityToRecord(roleEntity), ROLES.ROLE_ID);
    }
}
