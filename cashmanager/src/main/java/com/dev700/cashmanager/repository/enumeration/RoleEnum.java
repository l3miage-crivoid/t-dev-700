package com.dev700.cashmanager.repository.enumeration;


import com.dev700.cashmanager.jooq.generate.table.enums.Role;

public enum RoleEnum {
    ROLE_USER("ROLE_USER"),
    ROLE_ADMIN("ROLE_ADMIN");

    String name;

    RoleEnum(String name) {
        this.name = name;
    }

    public static RoleEnum convert(Role role) {
        switch (role.getLiteral()) {
            case "ROLE_ADMIN": return RoleEnum.ROLE_ADMIN;
            default: return RoleEnum.ROLE_USER;
        }
    }
}
