package com.dev700.cashmanager.repository.impl;

import com.dev700.cashmanager.repository.BankAccountRepository;
import com.dev700.cashmanager.entity.BankAccountEntity;
import com.dev700.cashmanager.jooq.context.VmDSLContext;
import com.dev700.cashmanager.jooq.generate.table.tables.records.BankAccountRecord;

import com.dev700.cashmanager.mapper.AccountMapper;
import org.springframework.stereotype.Repository;

import static com.dev700.cashmanager.jooq.generate.table.tables.BankAccount.BANK_ACCOUNT;
@Repository
public class BankAccountRepositoryImpl implements BankAccountRepository {
    private final VmDSLContext dslContext;
    private final AccountMapper accountMapper;

    public BankAccountRepositoryImpl(VmDSLContext dslContext, AccountMapper accountMapper) {
        this.dslContext = dslContext;
        this.accountMapper = accountMapper;
    }

    @Override
    public BankAccountEntity getBankAccountByUid(String uid) {
        return dslContext.selectFrom(BANK_ACCOUNT)
                .where(BANK_ACCOUNT.UID.eq(uid))
                .fetchOne(bankAccountRecord -> accountMapper.recordToEntity(bankAccountRecord.into(BankAccountRecord.class)));
    }

    @Override
    public void upsertBankAccountByID(BankAccountEntity bankAccountEntity) {
        dslContext.upsert(BANK_ACCOUNT, accountMapper.entityToRecord(bankAccountEntity), BANK_ACCOUNT.ID);
    }
}
