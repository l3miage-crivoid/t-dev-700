package com.dev700.cashmanager.repository;

import com.dev700.cashmanager.entity.RoleEntity;

import java.util.List;

public interface RoleRepository {
    List<RoleEntity> getRolesByUserId(Long userId);

    void upsertRole(RoleEntity roleEntity);
}
