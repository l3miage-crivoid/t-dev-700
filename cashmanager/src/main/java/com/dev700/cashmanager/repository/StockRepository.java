package com.dev700.cashmanager.repository;

import com.dev700.cashmanager.entity.StockEntity;

import java.util.List;

public interface StockRepository {
    /**
     * retrieves the stock
     * @return
     */
    List<StockEntity> getStock();
}
