package com.dev700.cashmanager.repository.impl;

import com.dev700.cashmanager.repository.StockRepository;
import com.dev700.cashmanager.entity.StockEntity;
import com.dev700.cashmanager.jooq.context.VmDSLContext;
import com.dev700.cashmanager.jooq.generate.table.tables.records.StockRecord;
import com.dev700.cashmanager.mapper.StockMapper;
import org.springframework.stereotype.Repository;

import java.util.List;

import static com.dev700.cashmanager.jooq.generate.table.Tables.STOCK;

@Repository
public class StockRepositoryImpl implements StockRepository {
    private final VmDSLContext dslContext;
    private final StockMapper stockMapper;

    public StockRepositoryImpl(VmDSLContext dslContext, StockMapper stockMapper) {
        this.dslContext = dslContext;
        this.stockMapper = stockMapper;
    }

    @Override
    public List<StockEntity> getStock() {
        return dslContext.selectFrom(STOCK)
                .fetch(record -> stockMapper.recordToEntity(record.into(StockRecord.class)));
    }
}
