package com.dev700.cashmanager.repository;

import com.dev700.cashmanager.entity.UserEntity;

import java.util.List;

public interface UserRepository {
    /**
     * retrieves users
     * @return
     */
    List<UserEntity> getUsers();

    /**
     * Retrieves a user entity based on the provided username.
     *
     * If a user is found, it fetches the associated roles from the 'ROLES' table using the user's ID.
     * These roles are then converted and set as authorities in the UserEntity object.
     *
     * @param username The username of the user to be retrieved.
     * @return A UserEntity object representing the user with the specified username.
     *         Returns null if no user is found with the given username.
     */
    UserEntity getUserByUsername(String username);

    /**
     * upsert user
     * @param userEntity
     */
    Long upsertUser(UserEntity userEntity);
}
