package com.dev700.cashmanager.repository;

import com.dev700.cashmanager.entity.BankAccountEntity;

public interface BankAccountRepository {

    /**
     * Retrieves a BankAccountEntity based on its unique identifier (UID).
     *
     * @param uid The unique identifier for the bank account.
     * @return BankAccountEntity corresponding to the provided UID. Returns null if no account is found.
     */
    BankAccountEntity getBankAccountByUid(String uid);

    /**
     * Inserts or updates a BankAccountEntity in the data store.
     * The operation (insert or update) depends on whether the entity's ID exists in the data store.
     *
     * @param bankAccountEntity The bank account entity to be inserted or updated.
     */
    void upsertBankAccountByID(BankAccountEntity bankAccountEntity);
}
