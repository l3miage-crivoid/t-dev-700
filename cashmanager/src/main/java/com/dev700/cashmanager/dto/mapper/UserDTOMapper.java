package com.dev700.cashmanager.dto.mapper;


import com.dev700.cashmanager.dto.UserDTO;
import com.dev700.cashmanager.entity.UserEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface UserDTOMapper {

    List<UserDTO> userEntityToUserDTO(List<UserEntity> userEntities);
}
