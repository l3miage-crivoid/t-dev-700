package com.dev700.cashmanager.dto.mapper;

import com.dev700.cashmanager.dto.StockDTO;
import com.dev700.cashmanager.entity.StockEntity;
import org.mapstruct.Mapper;
import org.mapstruct.NullValueCheckStrategy;

import java.util.List;

@Mapper(componentModel = "spring",
        nullValueCheckStrategy = NullValueCheckStrategy.ALWAYS)
public interface StockDTOMapper {
    List<StockDTO> stockEntitiesToStockDTO(List<StockEntity> userEntities);
}
