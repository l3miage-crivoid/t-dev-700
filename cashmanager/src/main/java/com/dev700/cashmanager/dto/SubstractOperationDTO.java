package com.dev700.cashmanager.dto;

public class SubstractOperationDTO {
    private String uid;
    private Double amountToSubstract;

    public String getUid() {
        return uid;
    }

    public SubstractOperationDTO setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public Double getAmountToSubstract() {
        return amountToSubstract;
    }

    public SubstractOperationDTO setAmountToSubstract(Double amountToSubstract) {
        this.amountToSubstract = amountToSubstract;
        return this;
    }
}
