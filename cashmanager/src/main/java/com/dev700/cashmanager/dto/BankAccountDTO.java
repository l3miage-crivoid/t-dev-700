package com.dev700.cashmanager.dto;

import java.math.BigDecimal;

public class BankAccountDTO {
    private Long id;

    private String uid;

    private String email;

    private Double amountOfMoney;

    public Long getId() {
        return id;
    }

    public BankAccountDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public BankAccountDTO setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public BankAccountDTO setEmail(String email) {
        this.email = email;
        return this;
    }

    public Double getAmountOfMoney() {
        return amountOfMoney;
    }

    public BankAccountDTO setAmountOfMoney(Double amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
        return this;
    }
}
