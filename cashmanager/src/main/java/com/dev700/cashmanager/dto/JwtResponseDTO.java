package com.dev700.cashmanager.dto;

import java.time.LocalDate;
import java.util.Set;

public class JwtResponseDTO {
    private String jwt;
    private Long id;
    private String username;
    private Set<String> roles;

    public JwtResponseDTO(String jwt, Long id, String username, Set<String> roles) {
        this.jwt = jwt;
        this.id = id;
        this.username = username;
        this.roles = roles;
    }

    public String getJwt() {
        return jwt;
    }

    public JwtResponseDTO setJwt(String jwt) {
        this.jwt = jwt;
        return this;
    }

    public Long getId() {
        return id;
    }

    public JwtResponseDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUsername() {
        return username;
    }

    public JwtResponseDTO setUsername(String username) {
        this.username = username;
        return this;
    }

    public Set<String> getRoles() {
        return roles;
    }

    public JwtResponseDTO setRoles(Set<String> roles) {
        this.roles = roles;
        return this;
    }
}
