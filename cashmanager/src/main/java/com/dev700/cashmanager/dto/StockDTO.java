package com.dev700.cashmanager.dto;

public class StockDTO {
    Long id;
    private String name;
    private Integer quantity;
    private Double price;

    public Long getId() {
        return id;
    }

    public StockDTO setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public StockDTO setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public StockDTO setQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public Double getPrice() {
        return price;
    }

    public StockDTO setPrice(Double price) {
        this.price = price;
        return this;
    }
}
