package com.dev700.cashmanager.dto;

public class TransactionResponseDTO {
    private String message;
    private Double amountToSubstract;

    public TransactionResponseDTO(String message , Double amountToSubstract) {
        this.message = message;
        this.amountToSubstract = amountToSubstract;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Double getAmountToSubstract() {
        return amountToSubstract;
    }

    public TransactionResponseDTO setAmountToSubstract(Double amountToSubstract) {
        this.amountToSubstract = amountToSubstract;
        return this;
    }
}
