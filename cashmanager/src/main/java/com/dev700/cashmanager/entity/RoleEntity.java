package com.dev700.cashmanager.entity;

import com.dev700.cashmanager.repository.enumeration.RoleEnum;

public class RoleEntity {
    private Long id;
    private Long userId;
    private RoleEnum roleName;

    public RoleEntity(Long userId, RoleEnum roleName) {
        this.userId = userId;
        this.roleName = roleName;
    }

    public Long getId() {
        return id;
    }

    public RoleEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public Long getUserId() {
        return userId;
    }

    public RoleEntity setUserId(Long userId) {
        this.userId = userId;
        return this;
    }

    public RoleEnum getRoleName() {
        return roleName;
    }

    public RoleEntity setRoleName(RoleEnum roleName) {
        this.roleName = roleName;
        return this;
    }
}
