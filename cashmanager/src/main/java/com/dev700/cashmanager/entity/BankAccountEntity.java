package com.dev700.cashmanager.entity;

public class BankAccountEntity {
    private Long id;

    private String uid;

    private String email;

    private Double amountOfMoney;

    public Long getId() {
        return id;
    }

    public BankAccountEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public String getUid() {
        return uid;
    }

    public BankAccountEntity setUid(String uid) {
        this.uid = uid;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public BankAccountEntity setEmail(String email) {
        this.email = email;
        return this;
    }

    public Double getAmountOfMoney() {
        return amountOfMoney;
    }

    public BankAccountEntity setAmountOfMoney(Double amountOfMoney) {
        this.amountOfMoney = amountOfMoney;
        return this;
    }
}
