package com.dev700.cashmanager.entity;

public class StockEntity {
    Long id;
    private String name;
    private Integer quantity;
    private Double price;

    public Long getId() {
        return id;
    }

    public StockEntity setId(Long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public StockEntity setName(String name) {
        this.name = name;
        return this;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public StockEntity setQuantity(Integer quantity) {
        this.quantity = quantity;
        return this;
    }

    public Double getPrice() {
        return price;
    }

    public StockEntity setPrice(Double price) {
        this.price = price;
        return this;
    }
}
