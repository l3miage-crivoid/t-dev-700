package com.dev700.cashmanager.controller;

import com.dev700.cashmanager.dto.SubstractOperationDTO;
import com.dev700.cashmanager.dto.TransactionResponseDTO;
import com.dev700.cashmanager.exception.BankAccountNotFoundException;
import com.dev700.cashmanager.entity.BankAccountEntity;
import com.dev700.cashmanager.service.BankAccountService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/bank")
public class BankController {
    private final BankAccountService bankAccountService;

    public BankController(BankAccountService bankAccountService) {
        this.bankAccountService = bankAccountService;
    }

    /**
     * Handles the POST request for processing a payment.
     *
     * @param substractOperationDTO Contains the information about the transaction, including the amount to subtract and the UID of the bank account.
     * @return ResponseEntity containing the transaction response details.
     */
    @PostMapping("/proceed-payment")
    @PreAuthorize("hasRole('ROLE_ADMIN') || hasRole('ROLE_USER') ")
    public ResponseEntity<TransactionResponseDTO> getBankAccountByUid(@RequestBody SubstractOperationDTO substractOperationDTO) {
        Double amountToSubstract = substractOperationDTO.getAmountToSubstract();

        try {
            // Retrieve the bank account using the UID provided in the request.
            BankAccountEntity bankAccount = bankAccountService.getBankAccountByUid(substractOperationDTO.getUid());

            // Attempt to subtract the specified amount from the bank account.
            Double amountSubstracted = bankAccountService.substractAmount(amountToSubstract , bankAccount);

            // If the transaction is successful, return a response entity with the transaction details.
            if(amountSubstracted != null){
                return ResponseEntity.ok(new TransactionResponseDTO("Transaction passed" , amountSubstracted));
            }

            // If the transaction fails due to insufficient funds, return a BAD_REQUEST response.
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new TransactionResponseDTO("Not enough money",amountToSubstract));
        } catch (BankAccountNotFoundException e) {
            // If the bank account is not found, return a NOT_FOUND response with the error message.
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(new TransactionResponseDTO(e.getMessage(),amountToSubstract));
        }
    }
}
