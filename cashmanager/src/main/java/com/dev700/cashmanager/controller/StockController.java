package com.dev700.cashmanager.controller;

import com.dev700.cashmanager.dto.StockDTO;
import com.dev700.cashmanager.dto.UserDTO;
import com.dev700.cashmanager.service.StockService;
import com.dev700.cashmanager.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api/stock")
public class StockController {
    private final StockService stockService;

    public StockController(StockService stockService) {
        this.stockService = stockService;
    }

    @GetMapping("/get-stock")
    @PreAuthorize("hasRole('ROLE_ADMIN') || hasRole('ROLE_USER') ")
    public ResponseEntity<List<StockDTO>> getStock() {
        return new ResponseEntity<>(stockService.getStock(), HttpStatus.OK);
    }
}
