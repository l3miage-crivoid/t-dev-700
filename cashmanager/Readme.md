Project Structure

Technologies:
Java 17 , liquibase , JOOQ , docker , jupiter , postgresql

Launch project :
1) launch database with "make run-db" you can pass env=prod
2) "make run" launch project in dockerized env for prod use env=prod

Description
Architecture: The project follows a typical Java application structure with a clear separation of concerns, evidenced by distinct packages for controllers, services, DTOs, entities, and repositories.
Functionality: It is designed for managing bank accounts, stocks, and user information, with corresponding services and controllers for each.
Data Layer: The usage of jOOQ suggests a SQL database backend.
Error Handling: Custom exceptions like BankAccountNotFoundException is a thoughtful approach to error handling.

Root Directory:

cashmanager/ - The main directory of the project.
Main Application:

CashmanagerApplication.java - The entry point of the application.
Configuration:

config/ - Contains configuration files.
DataSourceConfig.java - Configuration for the data source.
JooqConfig.java - Configuration for jOOQ (presumably for database interaction).
Controllers:

controller/ - Handles HTTP requests and responses.
BankController.java - Controller for bank-related operations.
StockController.java - Controller for stock-related operations.
UserController.java - Controller for user management.
Data Transfer Objects (DTOs):

dto/ - Contains DTOs for transferring data between processes.
BankAccountDTO.java
StockDTO.java
SubstractOperationDTO.java
TransactionResponseDTO.java
UserDTO.java
dto/mapper/ - Mappers for converting entities to DTOs and vice versa.
BankAccountDTOMapper.java
StockDTOMapper.java
UserDTOMapper.java
Entities:

entity/ - Represents the application's data model.
BankAccountEntity.java
StockEntity.java
UserEntity.java
Exceptions:

exception/ - Custom exceptions.
BankAccountNotFoundException.java
jOOQ Integration:

jooq/ - Integration with jOOQ for database interaction.
context/
VmDSLContext.java
Mappers:

mapper/ - Mappers for converting between entities and DTOs.
AccountMapper.java
StockMapper.java
UserMapper.java
Repositories:

repository/ - Data access layer.
BankAccountRepository.java
StockRepository.java
UserRepository.java
repository/impl/ - Implementations of the repositories.
BankAccountRepositoryImpl.java
StockRepositoryImpl.java
UserRepositoryImpl.java
Services:

service/ - Business logic.
BankAccountService.java
StockService.java
UserService.java
service/impl/ - Implementations of the services.
BankAccountServiceImpl.java
StockServiceImpl.java
UserServiceImpl.java
